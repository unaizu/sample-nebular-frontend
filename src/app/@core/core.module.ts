import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbAuthJWTToken, NbPasswordAuthStrategy, NB_AUTH_TOKEN_INTERCEPTOR_FILTER } from '@nebular/auth';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs';

import { throwIfAlreadyLoaded } from './module-import-guard';
import {
  AnalyticsService,
  LayoutService,
} from './utils';
import { UserData } from './data/users';
import { UserService } from './mock/users.service';
import { MockDataModule } from './mock/mock-data.module';
import { environment } from '../../environments/environment';

const socialLinks = [
  {
    url: 'https://natilik.com',
    target: '_blank',
    icon: 'link-2',
  },
  {
    url: 'https://www.facebook.com/natilikgroup/',
    target: '_blank',
    icon: 'facebook',
  },
  {
    url: 'https://twitter.com/natilikgroup/',
    target: '_blank',
    icon: 'twitter',
  },
];

const DATA_SERVICES = [
  { provide: UserData, useClass: UserService },
];

/*
export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}
*/

export const NB_CORE_PROVIDERS = [
  ...MockDataModule.forRoot().providers,
  ...DATA_SERVICES,

  ...NbAuthModule.forRoot({

    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        token: {
          class: NbAuthJWTToken, // assumes json response contains data.token
          key: 'token', // in our case it's returned as { token: 'tokenid' }
        },
        baseEndpoint: `${environment.api_url}${environment.api_version}`,
        login: {
          endpoint: '/auth/sign-in',
          method: 'post',
          redirect: {
            success: '/pages/dashboard',
            failure: null, // stay on the same page
          },
        },
        logout: {
          endpoint: '/auth/sign-out',
          method: 'post',
          redirect: {
            success: '/',
            failure: '/',
          },
        },
        refreshToken: {
          endpoint: '/auth/refresh-token',
          method: 'post',
          redirect: {
            success: '/',
            failure: '/',
          },
        },
      }),
    ],
    forms: {},
  }).providers,


  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
      },
      admin: {
        parent: 'user',
        remove: '*',
      }
    },
  }).providers,

/*
  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  */
  AnalyticsService,
  LayoutService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
