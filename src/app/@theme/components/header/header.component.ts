// import { WebsocketService } from './../../../services/websocket.service';
import { Router } from '@angular/router';
import { LogService } from './../../../utilities/log.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import { filter, map, takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { RippleService } from '../../../@core/utils/ripple.service';
import { environment } from '../../../../environments/environment';
import { NbAuthJWTToken, NbAuthService, NbTokenService } from '@nebular/auth'; // used for jwt token and auth
@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  public readonly materialTheme$: Observable<boolean>;
  userPictureOnly: boolean = false;
  user:any = {};

  notifiedChanges: string[] = [];
  alertBadge: string = '';
  messages: string[] = ['There are no messages'];
  messagesBadge: string = '';

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = [ { title: 'Log out' } ];

  appName = environment.applicationName;

  public constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    //private userService: UserData,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private rippleService: RippleService,
    private authService: NbAuthService,
    private logger: LogService,
    private tokenService: NbTokenService,
    private router: Router,
    // private wsService: WebsocketService,
  ) {
    this.materialTheme$ = this.themeService.onThemeChange()
      .pipe(map(theme => {
        const themeName: string = theme?.name || '';
        return themeName.startsWith('material');
      }));

      this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
        if (token.isValid()) {
          this.user = token.getPayload();
        }
      });

      this.menuService.onItemClick()
        .pipe(
          filter(({ tag }) => tag === 'user-auth-menu'),
          map(({ item: { title }}) => title ),
        )
        .subscribe(title => {
          this.logger.debug(`'${title}' was clicked!`);

          if (title === 'Log out') {
            this.logger.debug('logging out now!');

            this.authService.logout('email');
            this.tokenService.clear();
            this.router.navigate(['/auth/logout']);
          }
        });
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    /*
    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);
    */
    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => {
        this.currentTheme = themeName;
        this.rippleService.toggle(themeName?.startsWith('material'));
      });

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  openPopover(clear) {
    // if the user clicked the notification icon then clear the badge and empty the changes array
    this.alertBadge = '';
    if (clear) {
      this.notifiedChanges.length = 0;
    }
  }
}
