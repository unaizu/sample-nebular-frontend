import { NbPasswordAuthStrategy, NbAuthJWTToken, NB_AUTH_TOKEN_INTERCEPTOR_FILTER } from '@nebular/auth';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertService } from './utilities/alert.service';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './auth-guard.service';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import { LogService } from './utilities/log.service';
import { environment } from './../environments/environment';
import { TokenInterceptor } from './utilities/token-interceptor';
import { NbRoleProvider } from '@nebular/security';
import { RoleProviderService } from './services/role-provider.service';
import { NgxUploaderModule } from 'ngx-uploader';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    ThemeModule.forRoot(),

    NgxUploaderModule,

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    LogService,
    AlertService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true  }, // deps: [ClientsService]
    { provide: NbRoleProvider, useClass: RoleProviderService },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  // constructor(private authStrategy: NbPasswordAuthStrategy,
  //           private logger: LogService) {

  //     // const baseEndpoint = `${environment.api_url}${environment.api_version}`;
  //     // this.logger.debug('Base Endpoint:', baseEndpoint);

  //     // this.authStrategy.setOptions({
  //     //   name: 'email',
  //     //   token: {
  //     //     class: NbAuthJWTToken, // assumes json response contains data.token
  //     //     key: 'token', // in our case it's returned as { token: 'tokenid' }
  //     //   },
  //     //   baseEndpoint: baseEndpoint,
  //     //   login: {
  //     //     endpoint: '/auth/sign-in',
  //     //     method: 'post',
  //     //     redirect: {
  //     //       success: '/pages/dashboard',
  //     //       failure: null, // stay on the same page
  //     //     },
  //     //   },
  //     //   logout: {
  //     //     endpoint: '/auth/sign-out',
  //     //     method: 'post',
  //     //     redirect: {
  //     //       success: '/',
  //     //       failure: '/',
  //     //     },
  //     //   },
  //     //  // ... // initialize the strategy here
  //     // });
  // }
}
