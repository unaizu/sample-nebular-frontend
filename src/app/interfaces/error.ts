export interface Error {
  status?: number;
  error: {
    message?: string;
  };
}
