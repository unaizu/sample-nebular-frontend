import { MENU_ITEMS } from './pages-menu';
import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { NbAccessChecker } from '@nebular/security';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent implements OnInit {

  menu = MENU_ITEMS;

  constructor(private accessChecker: NbAccessChecker
    ) { }

  ngOnInit(): void {
    this.authMenuItems();

  }

  /**
   * Secures Menu items and associates them to the right permissions for NbUserAuth
   */
    authMenuItems() {
      this.menu.forEach(item => { // for each item on the menu secure it
        this.authMenuItem(item);
      });
    }

  authMenuItem(menuItem: NbMenuItem) {
    // if the menuItem has permission data
    if (menuItem.data && menuItem.data['permission'] && menuItem.data['resource']) {
      this.accessChecker.isGranted(menuItem.data['permission'], menuItem.data['resource']).subscribe(granted => {
        menuItem.hidden = !granted;
      });
    } else {
      menuItem.hidden = true;
    }
    if (!menuItem.hidden && menuItem.children != null) {
      menuItem.children.forEach(item => {
        if (item.data && item.data['permission'] && item.data['resource']) {
          this.accessChecker.isGranted(item.data['permission'], item.data['resource']).subscribe(granted => {
            item.hidden = !granted;
          });
        } else {
          // if child item do not config any `data.permission` and `data.resource` just inherit parent item's config
          item.hidden = menuItem.hidden;
        }
      });
    }
  }
}
