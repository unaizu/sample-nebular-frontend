import { PagesComponent } from './pages.component';
import { NbMenuModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { ThemeModule } from '../@theme/theme.module';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  imports: [
    PagesRoutingModule,
    NbMenuModule,
    ThemeModule,
  ],
  declarations: [
    PagesComponent,
    DashboardComponent,
  ],
})
export class PagesModule {}
