import { NbRoleProvider } from '@nebular/security';
import { Injectable } from '@angular/core';
import { LogService } from '../utilities/log.service';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RoleProviderService implements NbRoleProvider {

  constructor(private authService: NbAuthService,
            private logger: LogService) { }
/*
  getRole(): Observable<string> {
    return this.authService.onTokenChange()
      .pipe(
        map((token: NbAuthJWTToken) => {
          if (token.isValid()) {
            const role = token.getPayload()['role'];
            const admin = token.getPayload()['isAdmin'];
            console.log('the role and admin are:', role, admin);
            if (role) {
              this.logger.debug('user role:', role);
              return role;
            } else if (admin === true) {
              this.logger.debug('user role: admin');
              console.warn('remove this just testing')
              return 'guest';
              return 'admin';
            }
            this.logger.debug('user role: user');
            return 'user';
          }
        }),
      );
  }
*/
  getRole(): Observable<string> {
    return this.authService.onTokenChange()
      .pipe(
        map((token: NbAuthJWTToken) => {
/*
          if (token.isValid()) {
            const role = token.getPayload()['role'];
            const admin = token.getPayload()['isAdmin'];
            if (role) {
              this.logger.debug('[getRole] user role:', role);
              return role;
            } else if (admin === true) {
              // if the user is an admin then return admin role
              this.logger.debug('[getRole] user role: admin');
              console.warn('remove this');
              return 'guest';
              //return 'admin';
            }
          }
          // return guest if token is not valid
          this.logger.debug('[getRole] user role: guest');
          return 'guest';
*/
          if (token.isValid()) {
            const role = token.getPayload()['role'];
            const admin = token.getPayload()['isAdmin'];
            if (role) {
              this.logger.debug('[getRole] user role:', role);
              return role;
            } else if (admin === true) {
              // if the user is an admin then return admin role
              this.logger.debug('[getRole] user role: admin');
              //return 'guest';
              return 'admin';
            }
          }
          return 'guest';
          /*
          console.warn('got token', token);
          console.warn(token.getPayload()['role'])
          return token.isValid() ? token.getPayload()['role'] : 'guest';
          */
        }),
      );
  }
}
