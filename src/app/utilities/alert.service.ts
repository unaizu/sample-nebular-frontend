import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { Error } from '../interfaces/error';

@Injectable()
export class AlertService {

  constructor (private toastrService: NbToastrService) {}

  show(title, message, status, destroyByClick = true, duration = 3000) {
    this.toastrService.show(
      message,
      title,
      { limit: 3, status, destroyByClick, duration });
  }

  error(message, duration = 0, error: Error = { error: undefined }) {

    if ( error.status && error.status === 403) {
      // this might be a permissions issue
      message += '. Please verify that you have sufficient privileges to make changes with your system Administrator';
      const status = 'warning';
      this.toastrService.show(
        message,
        'ERROR',
        {limit: 3, status, duration });
    } else {
      const status = 'danger';
      this.toastrService.show(
        message,
        'ERROR',
        {limit: 3, status, duration });
    }
  }


  success(message) {
    const status = 'success';
    this.toastrService.show(
      message,
      'SUCCESS',
      {limit: 3, status });
  }

  warn(message, duration = 3000) {
    const status = 'warning';
    this.toastrService.show(
      message,
      'WARNING',
      {limit: 3, status, duration });
  }

  /*
  static showToastr(title, message, status) {
    show(message, title, { status });
  }*/

}
