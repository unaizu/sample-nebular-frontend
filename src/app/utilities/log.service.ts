import { Injectable } from '@angular/core';

@Injectable()
export class LogService {

  constructor () {}

  debug(msg: any, data?: any) {
      // tslint:disable-next-line: no-console
      console.log(`🟢 ${JSON.stringify({ msg: msg, data: data, time: new Date() })}`);
  }

  info(msg: any, data?: any) {
    console.info(`🔵 ${JSON.stringify({ msg: msg, data: data, time: new Date() })}`);
  }

  warn(msg: any, data?: any) {
    console.warn(`🟠 ${JSON.stringify({ msg: msg, data: data, time: new Date() })}`);
  }

  error(msg: any, data?: any) {

    console.error(`🔴 ${JSON.stringify({ msg: msg, data: data, time: new Date() })}`);
    // if the error is due to a jwt expiring then refresh the browser
    if (data && data.error && data.error.message === 'jwt expired') {
      console.warn('JWT Token Expired, refreshing application...');
      window.location.reload();
    }
  }
}
