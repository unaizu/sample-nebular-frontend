import { environment } from './../../environments/environment';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NB_AUTH_INTERCEPTOR_HEADER, NbAuthToken } from '@nebular/auth';
import { Inject, Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptor implements HttpInterceptor {


  private token: string;

  /*
  constructor(private authService: NbAuthService) {
    console.warn('constructing token interceptor')
    this.authService.onTokenChange().subscribe( token => {
      if (token.isValid()) {
        console.warn('we got a token and it is valid');
        this.token = token.getValue();
      }
    });
  }*/

  constructor(private injector: Injector,
    @Inject(NB_AUTH_INTERCEPTOR_HEADER) protected filter) {
  }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // apply an interceptor filter here if this is a refresh token request
    // to avoid an infinite request loop here
    const url = `${environment.api_url}${environment.api_version}`;
    if (req.url === `${url}/auth/refresh-token`) {
      console.warn('got refresh token request, we need to fix this here');
      //window.location.reload();
      return next.handle(req);
    }
    return this.authService.isAuthenticatedOrRefresh()
        .pipe(
            switchMap(authenticated => {
                if (authenticated) {
                    return this.authService.getToken().pipe(
                        switchMap((token: NbAuthToken) => {
                            const JWT = `${token.getValue()}`;
                            req = req.clone({
                                setHeaders: {
                                    'Content-Type': 'application/json; charset=utf-8',
                                    Accept: 'application/json',
                                    CurrentTime: Date.now().toString(),
                                    Authorization: `Bearer ${JWT}`,
                                },
                            });
                            return next.handle(req);
                        }),
                    )
                } else {
                    // Request is sent to server without authentication so that the client code
                    // receives the 401/403 error and can act as desired ('session expired', redirect to login, aso)
                    req = req.clone({
                      setHeaders: {
                          'Content-Type': 'application/json; charset=utf-8',
                          CurrentTime: Date.now().toString(),
                          Accept: 'application/json',
                      },
                    });
                    return next.handle(req);
                }
            }),
        )
  /*
    const userToken = 'secure-user-token';
    const modifiedReq = req.clone({
      headers: req.headers.set('x-access-token', this.token),
    });
    return next.handle(modifiedReq);
    */
  }

  protected get authService(): NbAuthService {
    return this.injector.get(NbAuthService);
  }
}
