export const environment = {
  production: true,
  applicationName: 'Sample Nebular Application',
  api_url: 'http://localhost:3005/api/',
  api_version: 'v1'
};
